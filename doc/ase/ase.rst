.. module:: ase
   :synopsis: ASE module

.. _ase:

=======
Modules
=======

Quick links:

.. list-table::

  * - :mod:`~ase.atom`
    - :mod:`~ase.atoms`
    - :mod:`~ase.calculators`
    - :mod:`~ase.constraints`
  * - :mod:`~ase.db`
    - :mod:`~ase.dft`
    - :mod:`~ase.data`
    - :mod:`~ase.ga`
  * - :mod:`~ase.gui`
    - :mod:`~ase.infrared`
    - :mod:`~ase.io`
    - :mod:`~ase.lattice`
  * - :mod:`~ase.md`
    - :mod:`~ase.neb`
    - :mod:`~ase.optimize`
    - :mod:`~ase.parallel`
  * - :mod:`~ase.phasediagram`
    - :mod:`~ase.phonons`
    - :mod:`~ase.lattice.spacegroup`
    - :mod:`~ase.structure`
  * - :mod:`~ase.lattice.surface`
    - :mod:`~ase.transport`
    - :mod:`~ase.thermochemistry`
    - :mod:`~ase.units`
  * - :mod:`~ase.utils`
    - :mod:`~ase.vibrations`
    - :mod:`~ase.visualize`
    -


.. seealso::

   * :ref:`tutorials`
   * :ref:`cli`
   * :epydoc:`Automatically generated documentation <ase>` (API)
   * :git:`Source code <>`
   * Presentation about ASE: :download:`ase-talk.pdf`


List of all modules:

.. toctree::
   :maxdepth: 2

   atom
   atoms
   units
   io
   gui/gui
   setup-overview
   optimize
   parallel
   visualize/visualize
   calculators/calculators
   constraints
   vibrations
   phonons
   phasediagram/phasediagram
   thermochemistry/thermochemistry
   infrared
   md
   dft/dft
   transport/transport
   data
   trajectory
   utils
   neighbors
   io/opls
   db/db
   ga
   neb
   dimermethod
